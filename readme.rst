django-keep-on-modify
=====================

I needed to keep all the updates to the models made by the views so I came up with this little hack that would make the code DRY. 

If you design your model while inheriting from ``patterns.models.GhostedModel`` all your updates will be stored along side with ``ghosted_at`` set to date and time of operation and ``ghosted_origin`` set to foreign key of original data. That way you can retrace all updates done to your data.

Please, note, the model will inherit the ``objects`` manager making the *ghosted* elements unaccesible to you.

**This code is consider broken and untested as I just ripped it from production to make it reusable**
