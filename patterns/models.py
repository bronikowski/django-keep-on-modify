# -*- coding: utf-8 -*-
from django.db import models
import datetime


class NonGhostedModel(models.Manager):

    def get_query_set(self):
        return super(NonGhostedModel, self).get_query_set().filter(ghosted_at__isnull=True)

class NonDeletedModel(models.Manager):

    def get_query_set(self):
        return super(NonDeletedModel, self).get_query_set().filter(deleted_at__isnull=True)

class GhostedModel(models.Model):
    objects = NonGhostedModel()
    ghosted_at = models.DateTimeField(null=True)
    ghosted_origin = models.ForeignKey('self',related_name='ghost',null=True)

    def save(self, *args, **kwargs):
        
        try:
            inherited = self.__bases__[0]
        except KeyError:
            pass # fixme

        if self.id and not self.ghosted_origin:
            ghost = inherited.objects.get(pk=self.id)
            ghost.ghosted_at = datetime.datetime.now()
            ghost.ghosted_origin = self
            ghost.id = None
            ghost.save()

        super(inherited, self).save(*args, **kwargs)


